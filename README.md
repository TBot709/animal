# animal

Adaptable, no inference, reinforcement machine learning.

A console version of a tic-tac-toe game is included here, which I used for testing. When an animal is named, and then plays some games, its memory is 
saved to a file. Naming the animal again next time, or in the training, will
result in its memory being loaded again. You can train your precious little
animals by pitting them against one another (the opponents memory is not
saved between training sessions). 

With the default parameters, the ai seems to quickly train for optimal moves
when they are playing as 'X' but struggles with defending as 'O'

It is interesting to have the ai only learn through playing a human, and
then notice when it eventually starts copying them!

If trained too long with a low playfulness the ai begins to favor specific,
and often unfavorable, moves. Try increasing the playfulness when training
to mitigate the ai's tendency for 'nearsightedness'

# details on Animal class, as found in the source file

    Adaptable, no inference, reinforcement machine learning.

    Animal< K extends Serializable >(int playfulness) 
    Animal< K extends Serializable >(BigInteger playfulness)
        where K is the type for memory 'states' or keys.
        Negative playfulness values are set to zero.

    Uses short and long term memories, with the long term memory record keeping
    track of 'weights' which are altered by the reward() and scold() functions.
    The short term memory determines which states:choice pairs will have their
    weights altered, this short memory is typically a record of choices made
    through a current round or game and rewarding and scolding involves a 
    decreasing weight change through past decisions. 

    The long term memory is a state:weights record of all states an Animal has
    been presented through all rounds or games played. All choose() calls 
    reference this long memory to see if there are any outstanding (high weight)
    choices. The highest is usually chosen, but a parameter of 'playfulness' is
    included which defines a range of weights from the max whose choices have 
    an equal chance of being chosen. 

    Summary of Methods:

    void setPlayfulness(int playfulness);
        If playfulness is below zero, it's set to zero.

    void setPlayfulness(BigInteger playfulness);
        If playfulness is below zero, it's set to zero.

    int choose(K state, int num_choices);
        Note that is the responsibility of the calling function to keep track
        of what the return (between 0 and num_choices - 1, inclusive) means.
        Choices are automatically added to short term memory.

    void observe(K state, int num_choices, int choice) 
        Can be used instead of choose() to build a short term memory if an 
        Animal is learning as a passive observer of another player.

    void resetShortMemory() 
        Prepare the Animal for a fresh round, clears short term memory

    void resetAllMemory() 
        Clears short and long term memory

    HashMap<K,ArrayList<BigInteger>> getLongMemory()
        Returns the longMemory HashMap, useful for sharing memories b/t Animals

    void addLongMemory(HashMap<K,ArrayList<BigInteger>> longMemory) 
        Takes a longMemory HashMap as a parameter and adds all
        weights to this Animal's corresponding states:weights. New weights are
        copied into this Animal's long memory as new entries. 
        
    ArrayList<BigInteger> getWeights(K state)
        Returns the ArrayList of weights corresponding to the given state.
        Returns null if the state is not in the long term memory.

    String toString() 
        Returns a String of the current long and short memories and statistics.
        There are various, public, supporting functions used by this:
        toString_longMemory(), toString_shortMemory(), toString_statistics(),
        longMemorySize(), shortMemorySize().

    void saveLongMemory(String filePath)
        throws IOException();
        Saves long term memory to a file.

    void loadLongMemory(String filePath)
        throws IOException();
        Loads long term memory from a file.

#        
        
**TBot709 - tbot709@gmail.com**

# 