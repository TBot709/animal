#!/bin/bash

DIR_SRC=./src
DIR_BIN=./bin

NAME_MAIN=Animal_TicTacToe
SCRIPT_RUN=animal_tictactoe.sh

if [ ! -d $DIR_BIN ]; then
    mkdir $DIR_BIN
fi

echo "Compiling ca to $DIR_BIN..."

javac -d $DIR_BIN -cp $DIR_BIN $DIR_SRC/animal/*.java &&
echo "." &&
javac -d $DIR_BIN -cp $DIR_BIN $DIR_SRC/*.java &&
echo "." &&
echo "#!/bin/bash" > $SCRIPT_RUN &&
echo ".." &&
echo "cd $DIR_BIN" >> $SCRIPT_RUN &&
echo "." &&
echo "java $NAME_MAIN" >> $SCRIPT_RUN &&
echo "SUCCESS!" &&
echo "You can now use 'bash $SCRIPT_RUN' to run the program"


