/**
    
    TBot709, tbot709@gmail.com
    
    Tic-Tac-Toe example using an Animal, reinforcement learning.
    
    I admit this is a bit messy.

*/

import tbot.animal.Animal;

import java.util.Random;

public class Animal_TicTacToe{
    static void p(String s){System.out.println(s);}
    
    static Animal<String> ai;
    static String s_name;
    static int playfulness = 10;
    static int reward_win = 7;
    static int reward_tie = 2;
    static int scold_lose = 7;
    
    public static void main(String[] args){
        ai = new Animal<String>(playfulness);
    
        p("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        p("@                                                      @");
        p("@    Animal plays Tic Tac Toe, by TBot709              @");
        p("@                                                      @");
        p("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        menuName(); 
        
        String s_input;
        while(true){
            p("@@@@@@@@@@@@@@@@@@@@ Main Menu @@@@@@@@@@@@@@@@@@@@@@@@@");
            p("             1. play against "+s_name);
            p("             2. train "+s_name);
            p("             3. display memory of "+s_name);
            p("             4. switch animal");
            p("             5. exit");
            p("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            s_input = System.console().readLine();
            if(s_input.length() == 0) continue;
            
            switch(s_input.charAt(0)){
                case 'p':
                case 'P':
                case '1': play(); break;
                case 't':
                case 'T':
                case '2': menuTrain(); break;
                case 'd':
                case 'D':
                case '3': p(ai.toString()); break;
                case 's':
                case 'S':
                case '4': menuName(); break;
                case 'x':
                case 'X':
                case 'e':
                case 'E':
                case '5': return;
            }
        }
    }
    
    
    static boolean isQuit;
    static boolean isGameOver;
    static boolean isXTurn;
    static boolean isHumanX;
    static void play(){
    
        pHelp();
        
        isQuit = false;
        while(!isQuit){
            p("Would you like to change parameters before you start (y/n)?");
            String s_input = System.console().readLine();
            if(s_input.length() == 0) continue;
            switch(s_input.charAt(0)){
                case 'n':
                case 'N': isQuit = true; break;
                case 'y':
                case 'Y': menuParams(); isQuit = true; break;
            }
        }
        
        Animal<String> observer = new Animal<>(playfulness);
        isQuit = false;
        String s_input;
        int humanwins = 0; 
        int aiwins = 0;
        int ties = 0;
        while(!isQuit){
            p("    "+humanwins+" = wins for human");
            p("    "+aiwins+" = wins for "+s_name);
            p("    "+ties+" = ties");
            
            isHumanX = ((new Random()).nextInt(2) == 0) ? true : false;    
            
            isGameOver = false;
            isXTurn = true;
            char[] board = "         ".toCharArray();
            int input;
            while(!isGameOver){
                
                if(isXTurn){
                    
                    if(isHumanX){
                        input = getHumanInput(board);
                        if(isQuit) break;
                        observer.observe(boardToState(board,'X'),numFreeChoices(board),input);
                    }else{
                        input = ai.choose(boardToState(board,'X'),numFreeChoices(board));
                    }
                    
                    int index = 0;
                    while(input >= 0){
                        if(board[index] == ' '){
                            input--;
                        }
                        index++;
                    }
                    
                    board[index - 1] = 'X';
                    
                    if(isAWin(board)){
                        if(isHumanX){
                            observer.reward(reward_win);
                            ai.scold(scold_lose);
                            p("\tYou Win!");
                            pBoard(board);
                            humanwins++;
                        }else{
                            ai.reward(reward_win);
                            observer.scold(scold_lose);
                            p("\t"+s_name+" Wins");
                            pBoard(board);
                            aiwins++;
                        }
                        isGameOver = true;
                    }else{
                        if(isATie(board)){
                            ai.reward(reward_tie);
                            p("\tTie Game");
                            pBoard(board);
                            ties++;
                            isGameOver = true;
                        }
                    }
                    
                }else{   //'O's turn
                
                    if(!isHumanX){
                        input = getHumanInput(board);
                        if(isQuit) break;
                        observer.observe(boardToState(board,'X'),numFreeChoices(board),input);
                    }else{
                        input = ai.choose(boardToState(board,'O'),numFreeChoices(board));
                    }
                    
                    int index = 0;
                    while(input >= 0){
                        if(board[index] == ' '){
                            input--;
                        }
                        index++;
                    }
                    
                    board[index - 1] = 'O';
                    
                    if(isAWin(board)){
                        if(!isHumanX){
                            observer.reward(reward_win);
                            ai.scold(scold_lose);
                            p("\tYou Win!");
                            pBoard(board);
                            humanwins++;
                        }else{
                            ai.reward(reward_win);
                            observer.scold(scold_lose);
                            p("\t"+s_name+" Wins");
                            pBoard(board);
                            aiwins++;
                        }
                        isGameOver = true;
                    }else{
                        if(isATie(board)){
                            ai.reward(reward_tie);
                            observer.reward(reward_tie);
                            p("\tTie Game");
                            pBoard(board);
                            ties++;
                            isGameOver = true;
                        }
                    }
                    
                }
                 
                isXTurn = !isXTurn; 
                 
            } //EO while(!isGameOver)
            
            ai.resetShortMemory();
            ai.addLongMemory(observer.getLongMemory());
            observer.resetAllMemory();
            
        } //EO while(!isQuit)
        
        try{
            ai.saveLongMemory(s_name+".animal");
        }catch(java.io.IOException e){
            p("Error saving memory for "+s_name);
            p(e.toString());
        }
        
        p("Memory of "+s_name+" saved");
        
    }
    static void pBoard(char[] board){
        p("\t\t---------");
        p("\t\t| "+board[0]+" "+board[1]+" "+board[2]+" |");
        p("\t\t| "+board[3]+" "+board[4]+" "+board[5]+" |");
        p("\t\t| "+board[6]+" "+board[7]+" "+board[8]+" |");
        p("\t\t---------");
    }
    static void pHelp(){
        p("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        p("    Keys to make moves (followed by enter):");
        p("                   7 8 9        u i o ");
        p("                   4 5 6        j k l ");
        p("                   1 2 3        m , . ");
        p("    Other Options:");
        p("        Help: display this message again");
        p("        Quit: save and return to the main menu");
        p("        Display: display memory of "+s_name);
        p("        Param: change parameters");
        p("    Current Parameters:");
        p("        playfulness = "+playfulness);
        p("        reward for winning = "+reward_win);
        p("        reward for tie = "+reward_tie);
        p("        scolding for losing = "+scold_lose);
        p("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    }
    static void menuParams(){
        int input;
        p("Enter integers for the following parameters.");
        while(true){
            p("Playfulness (must be positive):");
            try{
                input = Integer.parseInt(System.console().readLine());
            }catch(Exception e){
                continue;
            }
            if(input < 0) continue;
            playfulness = input;
            break;
        }
        ai.setPlayfulness(playfulness);
        while(true){
            p("Reward for winning:");
            try{
                input = Integer.parseInt(System.console().readLine());
            }catch(Exception e){
                continue;
            }
            reward_win = input;
            break;
        }
        while(true){
            p("Reward for tie:");
            try{
                input = Integer.parseInt(System.console().readLine());
            }catch(Exception e){
                continue;
            }
            reward_tie = input;
            break;
        }
        while(true){
            p("Scolding for loss:");
            try{
                input = Integer.parseInt(System.console().readLine());
            }catch(Exception e){
                continue;
            }
            scold_lose = input;
            break;
        }
    }
    static int getHumanInput(char[] board){
        while(true){
            if(isHumanX) p("\tYou are X this game. (H for help, Q to quit)");
            else p("\tYou are O this game. (H for help, Q to quit)");
            pBoard(board);
            String s_input = System.console().readLine();
                            
            if(s_input.length() == 0) continue;
            
            switch(s_input.charAt(0)){
                case 'H':
                case 'h': pHelp(); continue;
                case 'Q':
                case 'q': isGameOver = true; isQuit = true; return 9;
                case 'P':
                case 'p': menuParams(); pHelp(); continue;
                case 'd':
                case 'D': p(ai.toString()); continue;
            }
            int input = 0;
            try{
                input = Integer.parseInt(s_input);
            }catch(Exception e){
                switch(s_input.charAt(0)){
                    case 'U': 
                    case 'u': input = 7; break;
                    case 'I': 
                    case 'i': input = 8; break;
                    case 'O':
                    case 'o': input = 9; break;
                    case 'j':
                    case 'J': input = 4; break;
                    case 'k':
                    case 'K': input = 5; break;
                    case 'L':
                    case 'l': input = 6; break;
                    case 'M':
                    case 'm': input = 1; break;
                    case ',': 
                    case '<': input = 2; break;
                    case '.': 
                    case '>': input = 3; break;
                }
            }
            if(input > 9 || input <= 0) continue;
            
            //convert from num pad location to board index
            switch(input){
                case 7: input = 0; break;
                case 8: input = 1; break;
                case 9: input = 2; break;
                case 4: input = 3; break;
                case 5: input = 4; break;
                case 6: input = 5; break;
                case 1: input = 6; break;
                case 2: input = 7; break;
                case 3: input = 8; break;
                default: System.out.println("Invalid input"); input = 9;
            }
            
            if(board[input] != ' ') continue;
            
            //convert index to nth free space
            int i = 0, n = 0;
            while(i < input){
                if(board[i++] == ' '){
                    n++;
                }
            }
            
            return n;
        }
    }
    static Animal<String> opponent_original = new Animal<>(10); //always serious
    static String opponent_name = "A Beginner";
    static void menuTrain(){
        while(true){
            p("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            p("    Ready to train "+s_name);
            p("    They will train against "+opponent_name);
            p("    Memory changes of the opponent will not be saved.");
            p("    Chose an option:");
            p("        1. train");
            p("        2. change parameters");
            p("        3. give opponent name");
            p("        4. quit");
            p("    Current Parameters:");
            p("        playfulness = "+playfulness);
            p("        reward for winning = "+reward_win);
            p("        reward for tie = "+reward_tie);
            p("        scolding for losing = "+scold_lose);
            p("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
            String s_input = System.console().readLine();
            if(s_input.length() == 0) continue;
            
            switch(s_input.charAt(0)){
                case 't':
                case 'T':
                case '1': train(); break;
                case 'c':
                case 'C':
                case 'p':
                case 'P':
                case '2': menuParams(); break;
                case 'g':
                case 'G':
                case 'o':
                case 'O':
                case '3': menuOpponent(); break;
                case 'q':
                case 'Q':
                case '4': return;
            }
        }
    }
    static void menuOpponent(){
        while(true){
            p("What is the name of the opponent?");
            String s_input = System.console().readLine();
            if(s_input.length() == 0) continue;
            opponent_name = s_input;
            break;
        }
        p("Loading memories of '"+opponent_name+"'...");
        try{
            opponent_original.loadLongMemory(opponent_name+".animal");
        }catch(Exception e){
            p("No valid memories found for '"+opponent_name+"'");
            p(e.toString());
            opponent_name = "A Beginner";
            opponent_original = new Animal<>(10); //always serious
        }   
    }
    static void menuName(){
        ai = new Animal<>(playfulness);
        p("\nPlease Enter your Animal's name");
        s_name = System.console().readLine();
        p("Loading memories of '"+s_name+"'...");
        try{
            ai.loadLongMemory(s_name+".animal");
        }catch(Exception e){
            p("No valid memories found for '"+s_name+"'");
            p(e.toString());
        }
    }
    static void train(){
        int num_games = 0;
        int wins = 0;
        int losses = 0;
        int ties = 0;
        while(true){
            p("Train for how many rounds?");
            try{
                num_games = Integer.parseInt(System.console().readLine());
            }catch(Exception e){
                continue;
            }
            if(num_games < 0) continue;
            break;
        }
        
        Animal<String> observer = new Animal<>(playfulness);
        Animal<String> opponent = new Animal<>(playfulness);
        opponent.addLongMemory(opponent_original.getLongMemory());
        
        while(num_games > 0){
            boolean isAIX = ((new Random()).nextInt(2) == 0) ? true : false; 
            boolean isGameOver = false;
            boolean isXTurn = true;
            char[] board = "         ".toCharArray();
            int input;
            while(!isGameOver){
                
                if(isXTurn){
                    if(isAIX){
                        input = ai.choose(boardToState(board,'X'),numFreeChoices(board));
                    }else{
                        input = opponent.choose(boardToState(board,'X'),numFreeChoices(board));
                        observer.observe(boardToState(board,'X'),numFreeChoices(board),input);
                    }
                    
                    int index = 0;
                    while(input >= 0){
                        if(board[index] == ' '){
                            input--;
                        }
                        index++;
                    }
                    
                    board[index - 1] = 'X';
                    
                    if(isAWin(board)){
                        if(isAIX){
                            ai.reward(reward_win);
                            opponent.scold(scold_lose);
                            observer.scold(scold_lose);
                            wins++;
                        }else{
                            opponent.reward(reward_win);
                            observer.reward(reward_win);
                            ai.scold(scold_lose);
                            losses++;
                        }
                        isGameOver = true;
                    }else{
                        if(isATie(board)){
                            ai.reward(reward_tie);
                            opponent.reward(reward_tie);
                            observer.reward(reward_tie);
                            ties++;
                            isGameOver = true;
                        }
                    }
                    
                }else{   //'O's turn
                
                    if(!isAIX){
                        input = ai.choose(boardToState(board,'X'),numFreeChoices(board));
                    }else{
                        input = opponent.choose(boardToState(board,'X'),numFreeChoices(board));
                        observer.observe(boardToState(board,'X'),numFreeChoices(board),input);
                    }
                    
                    int index = 0;
                    while(input >= 0){
                        if(board[index] == ' '){
                            input--;
                        }
                        index++;
                    }
                    
                    board[index - 1] = 'O';
                    
                    if(isAWin(board)){
                        if(!isAIX){
                            ai.reward(reward_win);
                            opponent.scold(scold_lose);
                            observer.scold(scold_lose);
                            wins++;
                        }else{
                            opponent.reward(reward_win);
                            observer.reward(reward_win);
                            ai.scold(scold_lose);
                            losses++;
                        }
                        isGameOver = true;
                    }else{
                        if(isATie(board)){
                            ai.reward(reward_tie);
                            opponent.reward(reward_tie);
                            observer.reward(reward_tie);
                            ties++;
                            isGameOver = true;
                        }
                    }
                    
                }
                 
                isXTurn = !isXTurn; 
                 
            } //EO while(!isGameOver)
            
            opponent.resetShortMemory();
            ai.resetShortMemory();
            
            ai.addLongMemory(observer.getLongMemory());
            observer.resetAllMemory();
            
            num_games--;
        }
        
        p("Training complete");
        p("Wins: "+wins+" Losses: "+losses+" Ties: "+ties);
        
        try{
            ai.saveLongMemory(s_name+".animal");
        }catch(Exception e){
            p("error saving memory for "+s_name);
            p(e.toString());
        }
        
        p("Memory of "+s_name+" saved");
    }
    
    static int numFreeChoices(char[] board){
        int numFree = 0;
        for(char c : board){
            if(c == ' ') numFree++;
        }
        return numFree;
    }
    
    static String boardToState(char[] board, char currentChar){
        char[] a_c = "         ".toCharArray();
        for(int i = 0; i < board.length; i++){
            if(board[i] == ' ') continue;
            if(board[i] == currentChar){
                a_c[i] = '1';
            }else{
                a_c[i] = '0';
            }
        }
        return new String(a_c);
    }
    
    static final String[] A_S_WINNING_COMBOS =
            {"100100100","010010010","001001001","111000000",
            "000111000","000000111","100010001","001010100"};
    static boolean isAWin(char[] board){
        for(String s : A_S_WINNING_COMBOS){
            int hits = 0;
            for(int i = 0; i < board.length; i++){
                if(s.charAt(i) == '1' && board[i] == 'X'){
                    hits++;
                }
            }
            if(hits == 3) return true;
            
            hits = 0;
            for(int i = 0; i < board.length; i++){
                if(s.charAt(i) == '1' && board[i] == 'O'){
                    hits++;
                }
            }
            if(hits == 3) return true;
        }
    
        return false;
    }
    
    static boolean isATie(char[] board){
        for(char c : board){
            if(c == ' ') return false;
        }
        return true;
    }
}
