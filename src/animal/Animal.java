package tbot.animal;

/** Animal

    TBot709, tbot709@gmail.com

    Adaptable, no inference, reenforcement machine learning.

    Animal< K extends Serializable >(int playfulness) 
    Animal< K extends Serializable >(BigInteger playfulness)
        where K is the type for memory 'states' or keys.
        Negative playfulness values are set to zero.
    
    Uses short and long term memories, with the long term memory record keeping
    track of 'weights' which are altered by the reward() and scold() functions.
    The short term memory determines which states:choice pairs will have their
    weights altered, this short memory is typically a record of choices made
    through a current round or game and rewarding and scolding involves a 
    decreasing weight change through past decisions. 
    
    The long term memory is a state:weights record of all states an Animal has
    been presented through all rounds or games played. All choose() calls 
    reference this long memory to see if there are any outstanding (high weight)
    choices. The highest is usually chosen, but a parameter of 'playfulness' is
    included which defines a range of weights from the max whose choices have 
    an equal chance of being chosen. 
    
    Summary of Methods:
    
    void setPlayfulness(int playfulness);
        If playfulness is below zero, it's set to zero.
    
    void setPlayfulness(BigInteger playfulness);
        If playfulness is below zero, it's set to zero.
    
    int choose(K state, int num_choices);
        Note that is the responsibility of the calling function to keep track
        of what the return (between 0 and num_choices - 1, inclusive) means.
        Choices are automatically added to short term memory.
    
    void observe(K state, int num_choices, int choice) 
        Can be used instead of choose() to build a short term memory if an 
        Animal is learning as a passive observer of another player.
    
    void resetShortMemory() 
        Prepare the Animal for a fresh round, clears short term memory
    
    void resetAllMemory() 
        Clears short and long term memory
    
    HashMap<K,ArrayList<BigInteger>> getLongMemory()
        Returns the longMemory HashMap, useful for sharing memories b/t Animals
    
    void addLongMemory(HashMap<K,ArrayList<BigInteger>> longMemory) 
        Takes a longMemory HashMap as a parameter and adds all
        weights to this Animal's corresponding states:weights. New weights are
        copied into this Animal's long memory as new entries. 
        
    ArrayList<BigInteger> getWeights(K state)
        Returns the ArrayList of weights corresponding to the given state.
        Returns null if the state is not in the long term memory.
    
    String toString() 
        Returns a String of the current long and short memories and statistics.
        There are various, public, supporting functions used by this:
        toString_longMemory(), toString_shortMemory(), toString_statistics(),
        longMemorySize(), shortMemorySize().
    
    void saveLongMemory(String filePath)
        throws IOException();
        Saves long term memory to a file.
    
    void loadLongMemory(String filePath)
        throws IOException();
        Loads long term memory from a file.
    
*/

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Random;
import java.math.BigInteger;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.Serializable;

public class Animal<K extends Serializable>{
    BigInteger playfulness;
    HashMap<K,ArrayList<BigInteger>> longMemory;
    class Decision{
        K state;
        int choice;
        
        public Decision(K  state, int choice){
            this.state = state;
            this.choice = choice;
        }
    }
    ArrayList<Decision> shortMemory;

    public Animal(int playfulness){
        if(playfulness < 0) playfulness = 0;
        this.playfulness = new BigInteger("" + playfulness);
        longMemory = new HashMap<>();
        shortMemory = new ArrayList<>();
    }
    public Animal(BigInteger playfulness){
        BigInteger zero = new BigInteger("0");
        if(playfulness.compareTo(zero) < 0) playfulness = zero;
        this.playfulness = playfulness;
        longMemory = new HashMap<>();
        shortMemory = new ArrayList<>();
    }
    
    public void setPlayfulness(int playfulness){
        if(playfulness < 0) playfulness = 0;
        this.playfulness = new BigInteger(playfulness + "");
    }
    public void setPlayfulness(BigInteger playfulness){
        BigInteger zero = new BigInteger("0");
        if(playfulness.compareTo(zero) < 0) playfulness = zero;
        this.playfulness = playfulness;
    }
    
    /** choose(K state, int num_choices)
        Searches the longMemory for the key matching states. 
        
        If no entry exists, a set of zero weights (size = num_choices) is 
        entered into the longMemory with the given state as key. In this case a
        random number between 0 and num_choices - 1 (inclusive) is then returned
        
        If the entry exists, the associated array of weights is searched for the
        largest. Then all weights are compared to this weight and a record is
        kept of the indexes for weights within the 'playful' range from the max.
        The one or more indexes (choices) found within the 'playful' range are
        chosen between randomly for the return.
        
        NOTE: This effectively returns a number between 0 and num_choices - 1.
        This is sometimes random, but as weights are accumulated, returns become
        more consistent. Remember that there is no set correspondence between 
        choices made and the state. The weights are solely determined by past
        behavior. Because of this, it is the responsibility of the methods
        using this Animal to be consistent when interpreting the returns of
        this function. 
    */
    public int choose(K state, int num_choices){
        //if state doesn't exist in memory map, create new entry
        if(!longMemory.containsKey(state)){
            ArrayList<BigInteger> al = new ArrayList<>();
            for(int i = 0; i < num_choices; i++){
                al.add(new BigInteger("0"));
            }
            longMemory.put(state,al);
            //return random value
            int rand = (new Random()).nextInt(num_choices);
            shortMemory.add(new Decision(state,rand));
            return rand;
        }
        
        //find largest weight
        BigInteger maxWeight = new BigInteger("0");
        ArrayList<BigInteger> choiceWeights = longMemory.get(state);
        if(choiceWeights.size() > 0){
            maxWeight = choiceWeights.get(0);
        }
        for(int i = 1; i < choiceWeights.size(); i++){
            if(choiceWeights.get(i).compareTo(maxWeight) > 0){
                maxWeight = choiceWeights.get(i);
            }
        }
        
        //find all choices within playful range of largest
        ArrayList<Integer> validChoices = new ArrayList<>();
        BigInteger bint_buff;
        for(int i = 0; i < choiceWeights.size(); i++){
            if(absDiff(choiceWeights.get(i),maxWeight).compareTo(playfulness) < 0){
                validChoices.add(i);
            }
        }
        
        //choose randomly from valid choices
        int choice = 
                validChoices.get((new Random()).nextInt(validChoices.size()));
        if(choice >= num_choices){ //memory has more entries that poss. choices?
            choice = (new Random()).nextInt(num_choices);
        }
        
        //add choice to short term memory
        shortMemory.add(new Decision(state,choice));
        
        return choice;
    }
    static BigInteger absDiff(BigInteger i, BigInteger j){
        BigInteger m = i.abs();
        BigInteger n = j.abs();
        if(m.compareTo(n) > 0) return m.subtract(n);
        else return n.subtract(m);
    }
    
    /** observe(K state,int num_choices, int choice)
        Used when an animal is not making choices, but observes
        another player's moves and remembers them.
    */
    public void observe(K state, int num_choices, int choice){
        //if state doesn't exist in memory map, create new entry
        if(!longMemory.containsKey(state)){
            ArrayList<BigInteger> al = new ArrayList<>();
            for(int i = 0; i < num_choices; i++){
                al.add(new BigInteger("0"));
            }
            longMemory.put(state,al);
        }
    
        shortMemory.add(new Decision(state,choice));
    }
    
    /** reward(int weight)
        the full reward weight is added to the longMemory entry corresponding to
        the most recent shortMemory choice. The size is then decremented by
        one and added to the longMemory entry corresponding to the next previous 
        shortMemory. This continues till either the size reaches zero, or there
        are no more previous shortMemory entries.
    */
    public void reward(int weight){
        //negative rewards are a scolding
        if(weight < 0){
            scold(weight);
            return;
        }
        
        int i = shortMemory.size() - 1;
        BigInteger weight_buff;
        Decision decision;
        ArrayList<BigInteger> al_weights;
        while(weight > 0){
            if(i < 0) break;
            
            decision = shortMemory.get(i);
            al_weights = longMemory.get(decision.state);
            weight_buff = al_weights.get(decision.choice);
            weight_buff = weight_buff.add(new BigInteger(weight + ""));
            al_weights.set(decision.choice,weight_buff);
            
            weight--;
            i--;
        }
    }
    
    /** scold(int weight)
        the full reward weight is subtracted from the longMemory entry 
        corresponding to the most recent shortMemory choice. The weight is then 
        decremented by one and subtracted from the longMemory entry 
        corresponding to the next previous shortMemory. This continues till 
        either the weight reaches zero, or there are no more previous shortMemory 
        entries.
    */
    public void scold(int weight){
        //negative scoldings are rewards
        if(weight < 0){
            reward(weight);
            return;
        }
    
        int i = shortMemory.size() - 1;
        BigInteger weight_buff;
        Decision decision;
        ArrayList<BigInteger> al_weights;
        while(weight > 0){
            if(i < 0) break;
            
            decision = shortMemory.get(i);
            al_weights = longMemory.get(decision.state);
            weight_buff = al_weights.get(decision.choice);
            weight_buff = weight_buff.subtract(new BigInteger(weight + ""));
            al_weights.set(decision.choice,weight_buff);
            
            weight--;
            i--;
        }
    }
    
    /** resetShortMemory()
        Erases short term memory
    */
    public void resetShortMemory(){
        shortMemory = new ArrayList<>();
    }
    
    /** resetAllMemory()
        Erases long and short term memory
    */
    public void resetAllMemory(){
        shortMemory = new ArrayList<>();
        longMemory = new HashMap<>();
    }
    
    /** getLongMemory()
        returns longMemory of this Animal
    */
    public HashMap<K,ArrayList<BigInteger>> getLongMemory(){
        return this.longMemory;
    }
    
    /** addLongMemory()
        adds weights of the given longMemory to this.longMemory
    */
    public void addLongMemory(HashMap<K,ArrayList<BigInteger>> longMemory){
        if(this.longMemory == longMemory) return; //avoid concurrent error
        
        ArrayList<BigInteger> al_this_weights, al_param_weights;
        for(K state : longMemory.keySet()){
            if(!this.longMemory.containsKey(state)){ //if entry is new
                this.longMemory.put(state,longMemory.get(state));
            }else{ //else already exists
                al_this_weights = this.longMemory.get(state);
                al_param_weights = longMemory.get(state);
                
                BigInteger n_buff;
                for(int i = 0; i < al_this_weights.size(); i++){
                    n_buff = al_this_weights.get(i);
                    n_buff = n_buff.add(al_param_weights.get(i));
                    al_this_weights.remove(i);
                    al_this_weights.add(i,n_buff);
                }
                
                this.longMemory.remove(state);
                this.longMemory.put(state,al_this_weights);
            }
        }
    }
    
    /** getWeights()
        Returns the ArrayList<BigInteger> corresponding to the given state
    */
    public ArrayList<BigInteger> getWeights(K state){
        if(!this.longMemory.containsKey(state)) return null;
        return this.longMemory.get(state);
    }
    
    /** toString():
        String of full contents of long term memory (state:weights), followed by
        contents of short term memory (state:choice), followed by some stats.
    */
    public String toString(){
        String s_return = "Long Memory:\n";
        s_return += toString_longMemory();
        
        s_return += "Short Memory:\n";
        s_return += toString_shortMemory();
        
        s_return += "Statistics:\n";
        s_return += toString_statistics();
        
        return s_return;
    }
    public String toString_longMemory(){
        String s_return = "";
        for(K key : longMemory.keySet()){
            s_return += key.toString() + ": ";
            for(BigInteger i : longMemory.get(key)){
                s_return += i + " ";
            }
            s_return += "\n";
        }
        return s_return;
    }
    public String toString_shortMemory(){
        String s_return = "";
        for(Decision decision : shortMemory){
            s_return += decision.state + ": " + decision.choice + "\n";
        }
        return s_return;
    }
    public String toString_statistics(){
        String s_return = "Long Memory Entries: "+longMemorySize()+"\n";
        s_return += "Short Memory Entries: "+shortMemorySize()+"\n";
        return s_return;
    }
    public int longMemorySize(){return longMemory.size();}
    public int shortMemorySize(){return shortMemory.size();}

    /** saveLongMemory(String filePath)
        Saves the long term memory HashMap to a file
    */
    public void saveLongMemory(String filePath)
            throws IOException{
        FileOutputStream fos = new FileOutputStream(filePath);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(longMemory);
        oos.close();
        fos.close();
    }
    
    /** loadLongMemory(String filePath)
        Loads a long term memory HashMap from a file
        
        "unchecked" warnings are suppressed as it is fair to assume the file
        we are looking for was created by the above function, so it should
        contain a HashMap of the same types. If not, a ClassCastException may
        be thrown, and this is simply thrown on inside a IOException.
    */
    @SuppressWarnings("unchecked")
    public void loadLongMemory(String filePath)
            throws IOException{
        FileInputStream fis = new FileInputStream(filePath);
        ObjectInputStream ois = new ObjectInputStream(fis);
        try{
            longMemory = (HashMap<K,ArrayList<BigInteger>>)ois.readObject();
        }catch(ClassNotFoundException e){
            throw new IOException(e);
        }catch(ClassCastException e){
            throw new IOException(e);
        }
        fis.close();
        ois.close();
    }
    
}